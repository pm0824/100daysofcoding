'''
Given an array of strings, group anagrams together.
Example:
Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
Output:
[
  ["ate","eat","tea"],
  ["nat","tan"],
  ["bat"]
]
Note:
1. All inputs will be in lowercase.
2. The order of your output does not matter.
'''

#Solution: 

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        groupedWords = defaultdict(list) 
        # Put all anagram words together in a dictionary  
        # where key is sorted word 
        for word in strs: 
            groupedWords["".join(sorted(word))].append(word) 
            
        return groupedWords.values()
  